from glob import glob
from rasterio import features
import sys
import numpy as np
import logging
import traceback
from PIL import Image
import geopandas as gpd
import json
from s2cloudless import S2PixelCloudDetector
import rasterio
import argparse
import os


def sentinel2_to_cloud_mask_preprocess(x):
    """
    takes x in the format of the tif file and rescales it to the format that s2 cloudless expects.
    """
    # last channel is a 'quality assesment' channel rather than a sensor input
    # s2 cloudless also expects channels last and to be scaled to 0-1

    return x[:13, :, :].transpose(1, 2, 0)[None, ...] / 10000


def compute_cloud_mask_save(cp_path, x, profile, verbose=False):
    z = sentinel2_to_cloud_mask_preprocess(x)
    cloud_detector = S2PixelCloudDetector(
        threshold=0.4, average_over=4, dilation_size=2, all_bands=True
    )

    if verbose:
        print("detecting clouds")
    cloud_mask = cloud_detector.get_cloud_probability_maps(z)

    profile.update(count=1, compress="lzw", dtype="float64", blockxsize=256, blockysize=256)

    minblocksize = min(*x.shape[1:])
    while profile['blockxsize'] > minblocksize:
        profile.update(blockxsize=profile['blockxsize'] // 2)

    while profile['blockysize'] > minblocksize:
        profile.update(blockysize=profile['blockysize'] // 2)

    if verbose:
        logging.info("\twriting cloud mask")

    cloud_mask = cloud_mask.squeeze()
    with rasterio.open(cp_path, "w", **profile) as dst:
        dst.write(cloud_mask, 1)

    return cloud_mask


"""
Map from names in the floodmap to rasterised value of watermask (see function compute_water)

The meaning of the codes are: 
 {0: 'land', 1: 'flood', 2: 'hydro', 3: 'permanent_water_jrc'}

"""
CODES_FLOODMAP = {
    # CopernicusEMS (flood)
    'Flooded area': 1,
    'Not Applicable': 1,
    'Flood trace': 1,
    'Dike breach': 1,
    'Standing water': 1,
    'Erosion': 1,
    'River': 2,
    'Riverine flood': 1,
    # CopernicusEMS (hydro)
    'BH140-River': 2,
    'BH090-Land Subject to Inundation': 2,
    'BH080-Lake': 2,
    'BA040-Open Water': 2,
    # 'BA030-Island': 2, islands are excluded! see filter_land func
    'BH141-River Bank': 2,
    'BH130-Reservoir': 2,
    'BH141-Stream': 2,
    # UNOSAT
    "preflood water": 2,
    # "Flooded area": 1,  # 'flood water' DUPLICATED
    "flood-affected land / possible flood water": 1,
    # "Flood trace": 1,  # 'probable flash flood-affected land' DUPLICATED
    "satellite detected water": 1,
    # "Not Applicable": 1,  # unknown see document DUPLICATED
    "possible saturated, wet soil/ possible flood water": 1,
    "aquaculture (wet rice)": 1,
    "tsunami-affected land": 1,
    "ran of kutch water": 1,
    "maximum flood water extent (cumulative)": 1,
}


def compute_water(tiffs2, floodmap, use_permanent_water_jrc=True):
    """ Rasterise flood map and add JRC permanent water layer """
    logging.info("\tRasterize water mask")

    # floodmap = gpd.read_file(filename_floodmap)
    shapes_rasterise = ((g, CODES_FLOODMAP[w]) for g, w in floodmap[['geometry', 'w_class']].itertuples(index=False,
                                                                                                        name=None))

    with rasterio.open(tiffs2) as src_s2:
        water_mask = features.rasterize(shapes=shapes_rasterise, fill=0,
                                        out_shape=src_s2.shape,
                                        dtype=np.uint8,
                                        transform=src_s2.transform)

    filename_permanent_water = tiffs2.replace("/S2/", "/PERMANENTWATERJRC/")
    if use_permanent_water_jrc and os.path.exists(filename_permanent_water):
        logging.info("\t Adding permanent water")
        permament_water = rasterio.open(filename_permanent_water).read(1)

        # Set to permanent water only things misslabeled as land or flood
        water_mask[(permament_water == 3) & (water_mask <= 1)] = 3

        # Seasonal water (permanent_water == 2) will not be used
    else:
        if use_permanent_water_jrc:
            logging.warning("Permanent water %s not found" % filename_permanent_water)

    return water_mask


# Unosat classes are coded in the flood map as this
UNOSAT_CLASS_TO_TXT = {
    0: "preflood water",
    1: "Flooded area",  # 'flood water'
    2: "flood-affected land / possible flood water",
    3: "Flood trace",  # 'probable flash flood-affected land'
    4: "satellite detected water",
    5: "Not Applicable",  # unknown
    6: "possible saturated, wet soil/ possible flood water",
    9: "aquaculture (wet rice)",
    14: "tsunami-affected land",
    77: "ran of kutch water",
    99: "maximum flood water extent (cumulative)"
}


def generate_mask_clouds(tiffs2, use_permanent_water_jrc=True):
    """
    Given the register vector and the downloaded S2 image it generates all derived products:
    (cloudprob, watermask, floodmaps, meta, maskrgb and gt)

    :param tiffs2: S2 Image
    :param subset_root:
    :param use_permanent_water_jrc:
    :return:
    """
    filename_cloudmask = tiffs2.replace("/S2/", "/cloudprob/")
    filename_meta = tiffs2.replace("/S2/", "/meta/").replace(".tif", ".json")

    filename_floodmap = tiffs2.replace("/S2/", "/floodmaps/").replace(".tif", ".shp")

    s2_img = None
    clouds = None

    # Check if _edited file exists and use that
    filename_cloudmask_edited = filename_cloudmask.replace("/cloudprob/", "/cloudprob_edited/")
    if not os.path.exists(filename_cloudmask_edited) and not os.path.exists(filename_cloudmask):
        logging.info("\tComputing cloud mask")
        with rasterio.open(tiffs2) as src_s2:
            s2_img = src_s2.read()
            meta_s2_rasterio = src_s2.meta
            clouds = compute_cloud_mask_save(filename_cloudmask, s2_img,
                                             src_s2.profile)

        clouds = clouds.squeeze()

    assert os.path.exists(filename_floodmap), f"Flood extent map not found {filename_floodmap}"
    floodmap = gpd.read_file(filename_floodmap)

    logging.info("\tRecomputing stats meta")

    if s2_img is None:
        with rasterio.open(tiffs2) as src_s2:
            s2_img = src_s2.read()
            meta_s2_rasterio = src_s2.meta
    if clouds is None:
        if os.path.exists(filename_cloudmask_edited):
            clouds = rasterio.open(filename_cloudmask_edited).read(1)
        else:
            clouds = rasterio.open(filename_cloudmask).read(1)

    water_mask = compute_water(tiffs2, floodmap, use_permanent_water_jrc=use_permanent_water_jrc)
    # water_mask -> {0: land, 1: flood, 2: hydro, 3: permanentwaterjrc}

    invalids = np.all(s2_img == 0, axis=0)

    # Set cloudprobs to zero in invalid pixels
    clouds[invalids] = 0
    cloudmask = clouds > .5

    # Set watermask values for compute stats
    water_mask[invalids] = 0
    water_mask[cloudmask] = 0

    # Create gt mask {0: invalid, 1:land, 2: water, 3: cloud}
    gt = np.ones(water_mask.shape, dtype=np.uint8)
    gt[water_mask > 0] = 2
    gt[cloudmask] = 3
    gt[invalids] = 0

    # save gt and generate jpegs
    save_gt(gt, tiffs2, meta_s2_rasterio)


def generate_rgb(s2rst, tiffs2):
    """ generate rgb from S2 image if it does not exists """

    filename_mask_rgb = tiffs2.replace("/S2/", "/S2rgb/").replace(".tif", ".jpeg")
    if os.path.exists(filename_mask_rgb):
        # File exists do not recompute
        return

    s2img = s2rst.read((4, 3, 2))
    s2img_rgb = (np.clip(s2img / 2500, 0, 1).transpose((1, 2, 0)) * 255).astype(np.uint8)
    Image.fromarray(s2img_rgb).save(filename_mask_rgb)


def mask_to_rgb(mask, values, colors_cmap):
    """
    Given a 2D mask it assigns each value of the mask its corresponding color
    """
    assert len(values) == len(colors_cmap), "Values and colors should have same length {} {}".format(len(values), len(colors_cmap))

    mask_return = np.zeros(mask.shape[:2] + (3,), dtype=np.uint8)
    colores = np.array(np.round(colors_cmap*255), dtype=np.uint8)
    for i, c in enumerate(colores):
        mask_return[mask == values[i], :] = c

    return mask_return


def save_gt(gt, tiffs2, meta_s2_rasterio):
    """ save binary ground truth and rgb jpeg """

    gt_rgb = mask_to_rgb(gt, values=[0, 1, 2, 3],
                         colors_cmap=np.array([[0, 0, 0],
                                               [139, 64, 0],
                                               [0, 0, 139],
                                               [220, 220, 220]]) / 255)

    filename_mask_rgb = tiffs2.replace("/S2/", "/maskrgb/").replace(".tif", ".jpeg")
    Image.fromarray(gt_rgb).save(filename_mask_rgb)

    filename_mask_tif = tiffs2.replace("/S2/", "/gt/")
    meta_copy = meta_s2_rasterio.copy()
    meta_copy["count"] = 1
    meta_copy["dtype"] = 'uint8'
    with rasterio.open(filename_mask_tif, "w", **meta_copy) as src_new:
        src_new.write_band(1, gt)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='FDL Europe 2019 Disaster Management Generate GT and jpeg files')
    parser.add_argument("--worldfloods_root", type=str, default=".", help="path to WORLDFLOODS folder")
    parser.add_argument('--no_use_permanent_water_jrc',
                        help='Not use permanent water layer to generate the ground truth',
                        action="store_true")

    args = parser.parse_args()

    logging.basicConfig(level=logging.INFO,
                        format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s')

    worldfloods_root = args.worldfloods_root

    subsets = ["train", "val", "test"]
    for s in subsets:
        # Check all folders have been downloaded
        sroot = os.path.join(worldfloods_root, s)
        assert os.path.exists(sroot), f"not found {sroot} folder"
        for fol in ["meta", "gt", "floodmaps"]:
            fullfol = os.path.join(sroot, fol)
            assert os.path.exists(fullfol), f"not found {fullfol} folder"

        # Create folder for processing
        for fol in ["maskrgb", "S2rgb", "cloudprob"]:
            fullfol = os.path.join(sroot, fol)
            if not os.path.exists(fullfol):
                os.mkdir(fullfol)

        s2_files = sorted(glob(os.path.join(sroot, "S2/*.tif")))
        s2_files = s2_files[50:] + s2_files[:50]
        for _i, s2_tiff_path in enumerate(s2_files):
            logging.info("%d/%d %s" % (_i + 1, len(s2_files), s2_tiff_path))
            try:
                with rasterio.open(s2_tiff_path) as s2_rst:
                    generate_rgb(s2_rst, s2_tiff_path)

                generate_mask_clouds(s2_tiff_path,
                                     use_permanent_water_jrc=not args.no_use_permanent_water_jrc)
            except Exception:
                traceback.print_exc(file=sys.stdout)
