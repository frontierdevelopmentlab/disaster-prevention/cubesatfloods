from albumentations import PadIfNeeded, ShiftScaleRotate, Compose, GaussNoise, \
    MotionBlur, Flip, Normalize, RandomRotate90
from models.transformations import PerChannel, ToTensor, ResizeFactor


def get_augmentation(channel_mean, channel_std, input_size=256, downsampling_factor=1, augment=True, normalize=True):
    # Pad to a square

    transform_list = []
    if augment and (input_size > 0):
        transform_list.append(PadIfNeeded(input_size, input_size))

    # Downsample if required
    if downsampling_factor > 1.01:
        transform_list.append(ResizeFactor(downsampling_factor=downsampling_factor,
                                           always_apply=True))

    if normalize:
        transform_list.append(Normalize(mean=channel_mean, std=channel_std, max_pixel_value=1.0))

    # Other augmentation
    if augment:
        channel_jitter = ShiftScaleRotate(shift_limit=0.001, scale_limit=0.01, rotate_limit=0.01)

        transform_list += [PerChannel([channel_jitter]),
                           GaussNoise(var_limit=(1e-6, 1e-3), p=0.2),
                           MotionBlur(3),
                           Flip(),
                           RandomRotate90()]

    #if drop_channels:
    #    transform_list.append(ChannelDropout(channel_drop_range=(1, 3)))

    transform_list.append(ToTensor())

    return Compose(transform_list)


def get_augmentation_train(channel_mean, channel_std, input_size=256, downsampling_factor=1, augment=True, normalize=True):
    """
    Get transformation for train time, by default augmentation is applied.

    Images are initially converted to a square with sides of length `input_size`. If
    `downsampling_factor > 1, then the image is resized.

    The final result is returned as a normalised tensor.
    """
    return get_augmentation(channel_mean, channel_std, input_size, downsampling_factor, augment, normalize)


def get_augmentation_test(channel_mean, channel_std, input_size=256, downsampling_factor=1, normalize=True):
    """
    Get transformation for test time. Augmentation is not applied.

    Images are initially converted to a square with sides of length `input_size`. If
    `downsampling_factor > 1, then the image is resized.

    The final result is returned as a normalised tensor.
    """
    return get_augmentation(channel_mean, channel_std, input_size, downsampling_factor, augment=False,
                            normalize=normalize)