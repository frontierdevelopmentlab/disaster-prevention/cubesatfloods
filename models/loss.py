import torch
import torch.nn as nn
import torch.nn.functional as F


def dice_loss(pred, target, smooth=1.):
    if pred.dim() == 4:
        target_one_hot = torch.nn.functional.one_hot(target,
                                                     num_classes=pred.shape[1]).permute(0, 3, 1, 2)
        axis_red = (2, 3)
    elif pred.dim() == 3:
        target_one_hot = torch.nn.functional.one_hot(target,
                                                     num_classes=pred.shape[0]).permute(2, 0, 1)
        axis_red = (1, 2)

    pred = pred.contiguous()
    target_one_hot = target_one_hot.float().contiguous()

    intersection = (pred * target_one_hot).sum(dim=axis_red)

    loss = (1 - ((2. * intersection + smooth) /
                 (pred.sum(dim=axis_red) + target_one_hot.sum(dim=axis_red) + smooth)))

    return loss.mean()


def calc_loss(pred, target, bce_weight=0.5, weight=None, filter_weight=None):

    # BCE Loss
    criteria = torch.nn.CrossEntropyLoss(weight=weight)
    bce = criteria(pred, target)

    # Dice Loss
    # Perform spatial softmax over NxCxHxW
    dice = dice_loss(torch.softmax(pred, dim=1), target)

    # Weighted sum
    loss = bce * bce_weight + dice * (1 - bce_weight)

    # L1 norm of input filter
    if filter_weight is not None:
        loss += filter_weight.norm(1)

    return loss
